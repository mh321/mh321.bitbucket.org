---
title: "Get that date (part 2)"
date: "2016-09-27"
tags: 
    - "c++"
    - "performance-tuning"
---

# Retrospective
In the [previous installment]({{< relref "get_that_date_p1.md">}}) I described a naïve way of creating formatted timestamp string from Unix time in microseconds. Issues highlighted in the previous part:

1.  I rely on `C`/`C++` library functions. These black-box like calls cannot be effectively optimised or cleverly inlined.
1.  Output formatting is being done in two steps - first for the date part and then for the microseconds part
1.  We were returning a `std::string` which might be not necessary.

Let's start with the last and first points.

**EDIT:** Most recent code samples and reviewed benchmark code available [here](https://bitbucket.org/mh321/time-experiments/overview).

<!--more-->

# We don't need a `std::string`
Our timestamp format happens to have length of 25 which means that depending on the compiler it might or might not benefit from [SSO](https://github.com/elliotgoodrich/SSO-23/blob/master/README.md). In my case - GCC - the string is too big - and `std::string` will have to allocate. How can we work around this? Here is one of the possible solutions (one could make a variation of that using `std::string_view` if available):

```cpp
const char * BENCHMARK_NOINLINE format_naive_strftime_snprintf(std::chrono::microseconds micros)
{
    thread_local std::tm tm;
    thread_local char buffer[32];

    const auto sec = static_cast<time_t>(micros.count() / 1000000);
    const auto usec = micros.count() % 1000000;


    bool ok = gmtime_r(&sec, &tm);
    ok &= std::strftime(buffer, sizeof(buffer), "%Y%m%dT%H:%M:%S", &tm);
    ok &= std::snprintf(buffer + DATE_TIME_LENGTH, sizeof(buffer) - DATE_TIME_LENGTH, ".%06ldZ", usec) > 0;
    if(BOOST_UNLIKELY(!ok)) buffer[0] = '\0';
    return buffer;
}
```

It is not necessary to use `thread_local` and in some cases - it is **not** a good idea (e.g. small performance cost with dynamic linking and working with [predictable input]({{< relref "get_that_date_p4.md">}})). An alternative would be to create time-formatting objects and using them judiciously.

This *tiny* change gives a modest performance improvement:

```
Benchmark                                                Time           
--------------------------------------------------------------------------------------
bm_to_string_naive_strftime_snprintf/real_time/threads:1_mean                               942 ns        942 ns  
bm_to_string_naive_strftime_snprintf/real_time/threads:4_mean                               272 ns       1070 ns  

bm_format_naive_strftime_snprintf/real_time/threads:1_mean                                  762 ns        762 ns  
bm_format_naive_strftime_snprintf/real_time/threads:4_mean                                  223 ns        873 ns  
```

Also those allocations observed in the previous example are gone:

```
-   20.79%     0.98%  time_benchmarks  time_benchmarks     [.] bm_format_naive_strftime_snprintf
   - 19.81% bm_format_naive_strftime_snprintf
        9.71% __tz_convert
        3.51% __offtime
        1.35% format_naive_strftime_snprintf
        1.27% __strftime_internal
        1.05% __tzfile_compute
        0.66% _IO_vsnprintf
        0.53% __snprintf
        0.53% __lll_lock_wait_private
```

# Modern C++, `chrono` and `date`
Instead of using `gmtime_r` it might be a better idea to use the [`date`](https://github.com/HowardHinnant/date) when modern C++ is available. It is header only and compiler can most likely generate better code thanks to smart inlining. Let's alter the code to get rid of `gmtime_r` (leaving the output formatting as is for now):

```cpp
const char * BENCHMARK_NOINLINE format_date(std::chrono::microseconds micros)
{
    thread_local char buffer[32];
    thread_local std::tm tm;

    using namespace std::chrono;
    using namespace date;
    time_point<system_clock, microseconds> tp{micros};

    const auto dp = floor<days>(tp);
    const auto ymd = year_month_day(dp);
    const auto tod = make_time(tp - dp);
    const auto usec = micros % 1000000;

    tm.tm_sec   = tod.seconds().count();
    tm.tm_min   = tod.minutes().count();
    tm.tm_hour  = tod.hours().count();
    tm.tm_mday  = unsigned(ymd.day());
    tm.tm_mon   = unsigned(ymd.month()) - 1u;
    tm.tm_year  = int(ymd.year()) - 1900;

    bool ok = std::strftime(buffer, sizeof(buffer), "%Y%m%dT%H:%M:%S", &tm);
    ok &= std::snprintf(buffer + DATE_TIME_LENGTH, sizeof(buffer) - DATE_TIME_LENGTH, ".%06ldZ", usec.count()) > 0;

    if(BOOST_UNLIKELY(!ok)) buffer[0] = '\0';
    return buffer;
}
```

Again we observe a small improvement and this time we get rid of the discrepancy between the single-threaded and multi-threaded variant. This time we're not using functions that lock:

```
Benchmark                                                Time           
--------------------------------------------------------------------------------------
bm_to_string_naive_strftime_snprintf/real_time/threads:1_mean                               942 ns        942 ns  
bm_to_string_naive_strftime_snprintf/real_time/threads:4_mean                               272 ns       1070 ns  

bm_format_naive_strftime_snprintf/real_time/threads:1_mean                                  762 ns        762 ns  
bm_format_naive_strftime_snprintf/real_time/threads:4_mean                                  223 ns        873 ns  

bm_format_date/real_time/threads:1_mean                                                     689 ns        689 ns  
bm_format_date/real_time/threads:4_mean                                                     173 ns        691 ns  

-   14.44%     1.21%  time_benchmarks  time_benchmarks     [.] bm_formate_date
   - 13.23% bm_format_date
      - 6.09% format_date
           date::year_month_day::from_days
        3.77% date::year_month_day::from_days
        1.46% __strftime_internal
        0.73% _IO_vsnprintf
        0.61% __snprintf
```

How about a simple and pure solution (without `strftime`/`snprintf`)?

```cpp
std::string BENCHMARK_NOINLINE format_date_pure(std::chrono::microseconds micros)
{
    using namespace std::chrono;
    using namespace date;
    time_point<system_clock, microseconds> tp{micros};
    return format("%Y%m%dT%TZ", tp);
}
```

The code as written above turns out to be not performing as expected:
```
bm_format_date_pure/real_time/threads:1_mean                                               6396 ns       6396 ns
bm_format_date_pure/real_time/threads:4_mean                                               4469 ns      17877 ns
```

Not sure yet if that is the intended way of using the `date` library.


# Output formatting
In the last example a significant part is dedicated to output formatting - namely obtaining the `tm` structure that can be passed to `strftime`. What if we formatted the result ourselves?

## Naïve Boost Karma formatting
Boost Karma provides [extremely fast](http://zverovich.net/2013/09/07/integer-to-string-conversion-in-cplusplus.html) **general purpose** `int` to `string` formatting. Increasing our compilation time we can rewrite our function as:

```cpp
#include <boost/spirit/include/karma.hpp>

const char * BENCHMARK_NOINLINE format_date_karma1(std::chrono::microseconds micros)
{
    thread_local char buffer[32];

    using namespace std::chrono;
    using namespace date;
    time_point<system_clock, microseconds> tp{micros};

    const auto dp = floor<days>(tp);
    const auto ymd = year_month_day(dp);
    const auto tod = make_time(tp - dp);
    const auto usec = micros.count() % 1000000;

    using namespace boost::spirit;
    char * it = buffer;
    bool ok = karma::generate(it,
                    karma::right_align(4, '0')[karma::int_] <<
                    karma::right_align(2, '0')[karma::uint_] <<
                    karma::right_align(2, '0')[karma::uint_] << 'T' <<
                    karma::right_align(2, '0')[karma::int_] << ':' <<
                    karma::right_align(2, '0')[karma::int_] << ':' <<
                    karma::right_align(2, '0')[karma::int_] << '.' <<
                    karma::right_align(6, '0')[karma::int_] << 'Z',
            int(ymd.year()), unsigned(ymd.month()), unsigned(ymd.day()),
            tod.hours().count(), tod.minutes().count(), tod.seconds().count(), usec);

    if(BOOST_UNLIKELY(!ok)) buffer[0] = '\0';
    return buffer;
}
```

Or alternatively formatting just the numbers into the right place:

```cpp
const char * BENCHMARK_NOINLINE format_date_karma1(std::chrono::microseconds micros)
{
    thread_local char buffer[32];

    using namespace std::chrono;
    using namespace date;
    time_point<system_clock, microseconds> tp{micros};

    const auto dp = floor<days>(tp);
    const auto ymd = year_month_day(dp);
    const auto tod = make_time(tp - dp);
    const auto usec = micros.count() % 1000000;

    using namespace boost::spirit;
    char * it = buffer;
    bool ok = karma::generate(it,
                    karma::right_align(4, '0')[karma::int_] <<
                    karma::right_align(2, '0')[karma::uint_] <<
                    karma::right_align(2, '0')[karma::uint_] << 'T' <<
                    karma::right_align(2, '0')[karma::int_] << ':' <<
                    karma::right_align(2, '0')[karma::int_] << ':' <<
                    karma::right_align(2, '0')[karma::int_] << '.' <<
                    karma::right_align(6, '0')[karma::int_] << 'Z',
            int(ymd.year()), unsigned(ymd.month()), unsigned(ymd.day()),
            tod.hours().count(), tod.minutes().count(), tod.seconds().count(), usec);

    if(BOOST_UNLIKELY(!ok)) buffer[0] = '\0';
    return buffer;
}
```

Compiling takes much longer... and the results are a bit of a let down:

```
bm_to_string_naive_strftime_snprintf/real_time/threads:1_mean                               942 ns        942 ns  
bm_to_string_naive_strftime_snprintf/real_time/threads:4_mean                               272 ns       1070 ns  

bm_format_naive_strftime_snprintf/real_time/threads:1_mean                                  762 ns        762 ns  
bm_format_naive_strftime_snprintf/real_time/threads:4_mean                                  223 ns        873 ns  

bm_formate_date/real_time/threads:1_mean                                                    689 ns        689 ns  
bm_formate_date/real_time/threads:4_mean                                                    173 ns        691 ns  

bm_format_date_karma1/real_time/threads:1_mean                                             1041 ns       1041 ns  
bm_format_date_karma1/real_time/threads:4_mean                                              267 ns       1069 ns  

bm_format_date_karma2/real_time/threads:1_mean                                              988 ns        988 ns  
bm_format_date_karma2/real_time/threads:4_mean                                              245 ns        980 ns  
```

Maybe I'm using it wrong? In the next installment I'm going to try using [hand-rolled domain specific formatting]({{< relref "get_that_date_p3.md">}}).
