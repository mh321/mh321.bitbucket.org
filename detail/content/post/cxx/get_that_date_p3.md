---
title: "Get that date (part 3)"
date: "2016-10-02"
tags: 
    - "c++"
    - "performance-tuning"
---

# Retrospective

In the previous installments:

* ([part 1]({{< relref "get_that_date_p1.md">}})) - a naïve (`gmtime_r`) way of creating formatted timestamp string from Unix time in microseconds
* ([part 2]({{< relref "get_that_date_p2.md">}})) - performance improvements to the naïve solution:
    * avoided unnecessary allocations by not creating `std::string` objects
    * replaced `gmtime_r` with modern [`date`](https://github.com/HowardHinnant/date) 
    * experimented (and failed) with changing `strftime` and `snprintf`calls with Boost Karma based formatting

In this short note I'm going to try to improve the performance by using a custom hand-rolled number formatting.

<!--more-->

**EDIT:** Most recent code samples and reviewed benchmark code available [here](https://bitbucket.org/mh321/time-experiments/overview).

# Custom `int` to `string`
I want to format a given number as a zero-padded string of a given length. My numbers are:
1. 4 digit years (`dddd`, `d ∈ [0; 9]`)
1. 2 digit months, days, hours, minutes (`dd`, `d ∈ [0; 9]`)
1. 6 digit microsecond count (`dddddd`, `d ∈ [0; 9]`)

## Lookup table algorithm

We can construct a lookup table to speed up mapping two digit numbers into strings:

```cpp
constexpr static char HUNDRED_NUMBERS[200] = {
    '0','0','1','0','2','0','3','0','4','0','5','0','6','0','7','0','8','0','9','0',
    '0','1','1','1','2','1','3','1','4','1','5','1','6','1','7','1','8','1','9','1',
    '0','2','1','2','2','2','3','2','4','2','5','2','6','2','7','2','8','2','9','2',
    '0','3','1','3','2','3','3','3','4','3','5','3','6','3','7','3','8','3','9','3',
    '0','4','1','4','2','4','3','4','4','4','5','4','6','4','7','4','8','4','9','4',
    '0','5','1','5','2','5','3','5','4','5','5','5','6','5','7','5','8','5','9','5',
    '0','6','1','6','2','6','3','6','4','6','5','6','6','6','7','6','8','6','9','6',
    '0','7','1','7','2','7','3','7','4','7','5','7','6','7','7','7','8','7','9','7',
    '0','8','1','8','2','8','3','8','4','8','5','8','6','8','7','8','8','8','9','8',
    '0','9','1','9','2','9','3','9','4','9','5','9','6','9','7','9','8','9','9','9',
};
```

We can divide the problem of converting a number `n=...d₂d₁d₀` to `s="...d₂d₁d₀"`  into the following parts:

- calculate the `length` of the number `n`
- if `length==1` (`n=d₀`) then `s[0] = char(n) + '0'`
- if `length==2` (`n=d₁d₀`) then we can use the lookup table to figure out the the output:

```cpp
const auto idx = n * 2;
s[1] = HUNDRED_NUMBERS[idx];
s[0] = HUNDRED_NUMBERS[idx+1];
```

- otherwise (`n=...d₃d₂d₁d₀` ) we can still use the lookup table and then divide `n` by 100 to reduce the problem and repeat the procedure:

```cpp
const auto idx = (n % 100) * 2;
n /= 100;
s[length - 1] = HUNDRED_NUMBERS[idx + 1];
s[length - 2] = HUNDRED_NUMBERS[idx];
```

For example let `n=12345`:

```
s=*****
length=5

Step 1:
idx := 45 * 2;
n := 12345 / 100 = 123;
s[4] = 5;
s[3] = 4;

Step 2:
idx := 23 * 2;
n := 123 / 100 = 1
s[2] = 3
s[1] = 2

Step 1:
s[0] = 1
```

## Date formatting code
Sample implementation of general purpose `utoa` that also does the zero-padding from the left:

```cpp
inline bool utoa_lookup(char * buffer, int limit, int32_t n)
{
    // Precondition: n is a positive value
    const auto length = count_digits(n);
    if(BOOST_UNLIKELY(length > limit))
        return false;

    for(auto i = 0; i < limit - length; ++i)
        *(buffer+i) = '0';

    buffer += limit;

    while(n >= 100)
    {
        const auto i = (n % 100) << 1;
        n /= 100;
        *--buffer = HUNDRED_NUMBERS[i];
        *--buffer = HUNDRED_NUMBERS[i + 1];
    }
    if(n < 10)
    {
        *--buffer = char(n) + '0';
    }
    else
    {
        const auto i = n << 1;
        *--buffer = HUNDRED_NUMBERS[i];
        *--buffer = HUNDRED_NUMBERS[i + 1];
    }
    return true;
}
```

There are many ways of counting the number of digits, for example using bit twiddling with compiler built-ins:

```cpp
constexpr static int LENGTH_LOOKUP[] = {
    0,
    10,
    100,
    1000,
    10000,
    100000,
    1000000,
    10000000,
    100000000,
    1000000000
};

inline int count_digits(int32_t n)
{
    // Precondition: n is a positive value
    int t = (32 - __builtin_clz(n | 1)) * 1233 >> 12;
    return t - (n < LENGTH_LOOKUP[t]) + 1;
}
```


... or [clever loop unrolling](https://www.facebook.com/notes/facebook-engineering/three-optimization-tips-for-c/10151361643253920/):

```cpp
inline int count_digits_unroll(int32_t n)
{
    // Precondition int32_t is positive
    int_fast32_t result = 1;
    for (;;)
    {
        if (n < 10)    return result;
        if (n < 100)   return result + 1;
        if (n < 1000)  return result + 2;
        if (n < 10000) return result + 3;
        n /= 10000;
        result += 4;
    }
}
```

This is how we can plug this custom formatting into our code:

```cpp
const char * BENCHMARK_NOINLINE format_date_counting_lookup(std::chrono::microseconds micros)
{
    thread_local char buffer[32] = "20160926T18:12:32.123456Z\0";

    using namespace std::chrono;
    using namespace date;
    time_point<system_clock, microseconds> tp{micros};

    const auto dp = floor<days>(tp);
    const auto ymd = year_month_day(dp);
    const auto tod = make_time(tp - dp);
    const auto usec = micros.count() % 1000000;

    bool ok = utoa_lookup(buffer + YEAR_OFFSET, 4, int(ymd.year()));
    ok &= utoa_lookup(buffer + MONTH_OFFSET, 2, unsigned(ymd.month()));
    ok &= utoa_lookup(buffer + DAY_OFFSET, 2, unsigned(ymd.day()));
    ok &= utoa_lookup(buffer + HOURS_OFFSET, 2, tod.hours().count());
    ok &= utoa_lookup(buffer + MINUTES_OFFSET, 2, tod.minutes().count());
    ok &= utoa_lookup(buffer + SECONDS_OFFSET, 2, tod.seconds().count());
    ok &= utoa_lookup(buffer + USEC_OFFSET, 6, usec);

    if(BOOST_UNLIKELY(!ok)) buffer[0] = '\0';
    return buffer;
}
```

And this proves to be a very good choice:
```
bm_to_string_naive_strftime_snprintf/real_time/threads:1_mean                               942 ns        942 ns
bm_to_string_naive_strftime_snprintf/real_time/threads:4_mean                               272 ns       1070 ns

bm_format_naive_strftime_snprintf/real_time/threads:1_mean                                  762 ns        762 ns
bm_format_naive_strftime_snprintf/real_time/threads:4_mean                                  223 ns        873 ns

bm_formate_date/real_time/threads:1_mean                                                    689 ns        689 ns
bm_formate_date/real_time/threads:4_mean                                                    173 ns        691 ns

bm_format_date_karma1/real_time/threads:1_mean                                             1041 ns       1041 ns
bm_format_date_karma1/real_time/threads:4_mean                                              267 ns       1069 ns

bm_format_date_karma2/real_time/threads:1_mean                                              988 ns        988 ns
bm_format_date_karma2/real_time/threads:4_mean                                              245 ns        980 ns

bm_format_date_counting_lookup/real_time/threads:1_mean                                     156 ns        156 ns
bm_format_date_counting_lookup/real_time/threads:4_mean                                      41 ns        162 ns

-   97.84%     4.98%  time_benchmarks  time_benchmarks     [.] bm_format_date_counting_lookup
   - 92.86% bm_format_date_counting_lookup
      - 69.10% format_date_counting_lookup
           47.05% utoa_lookup
           3.07% date::year_month_day::from_days
        15.75% date::year_month_day::from_days
        8.01% utoa_lookup
```

# Further tuning
As a matter of fact we could avoid counting the number of digits in our custom `utoa` implementation. Why is that?
- We know how many digits we want in each place (e.g. 4 for years, 6 for microseconds, 2 for everything else).
- We know that every number should be zero padded if it uses less space than required.
- We could detect errors by checking if there are any digits left after writing the required number of digits.

Using the above logic I can write the following three helper functions:

```cpp
const char * BENCHMARK_NOINLINE format_date_counting_lookup(std::chrono::microseconds micros)
{
    thread_local char buffer[32] = "20160926T18:12:32.123456Z\0";

    using namespace std::chrono;
    using namespace date;
    time_point<system_clock, microseconds> tp{micros};

    const auto dp = floor<days>(tp);
    const auto ymd = year_month_day(dp);
    const auto tod = make_time(tp - dp);
    const auto usec = micros.count() % 1000000;

    bool ok = utoa_lookup(buffer + YEAR_OFFSET, 4, int(ymd.year()));
    ok &= utoa_lookup(buffer + MONTH_OFFSET, 2, unsigned(ymd.month()));
    ok &= utoa_lookup(buffer + DAY_OFFSET, 2, unsigned(ymd.day()));
    ok &= utoa_lookup(buffer + HOURS_OFFSET, 2, tod.hours().count());
    ok &= utoa_lookup(buffer + MINUTES_OFFSET, 2, tod.minutes().count());
    ok &= utoa_lookup(buffer + SECONDS_OFFSET, 2, tod.seconds().count());
    ok &= utoa_lookup(buffer + USEC_OFFSET, 6, usec);

    if(BOOST_UNLIKELY(!ok)) buffer[0] = '\0';
    return buffer;
}
```

And change the formatting code to:

```cpp
const char * BENCHMARK_NOINLINE format_date_not_counting_lookup(std::chrono::microseconds micros)
{
    thread_local char buffer[32] = "20160926T18:12:32.123456Z\0";

    using namespace std::chrono;
    using namespace date;
    time_point<system_clock, microseconds> tp{micros};

    const auto dp = floor<days>(tp);
    const auto ymd = year_month_day(dp);
    const auto tod = make_time(tp - dp);
    const auto usec = micros.count() % 1000000;

    bool ok = utoa_lookup_faster4(buffer + YEAR_OFFSET, int(ymd.year()));
    ok &= utoa_lookup_faster2(buffer + MONTH_OFFSET, unsigned(ymd.month()));
    ok &= utoa_lookup_faster2(buffer + DAY_OFFSET, unsigned(ymd.day()));
    ok &= utoa_lookup_faster2(buffer + HOURS_OFFSET, tod.hours().count());
    ok &= utoa_lookup_faster2(buffer + MINUTES_OFFSET, tod.minutes().count());
    ok &= utoa_lookup_faster2(buffer + SECONDS_OFFSET, tod.seconds().count());
    ok &= utoa_lookup_faster6(buffer + USEC_OFFSET, usec);

    if(BOOST_UNLIKELY(!ok)) buffer[0] = '\0';
    return buffer;
}
```

This gives yet another performance improvement:

```
bm_to_string_naive_strftime_snprintf/real_time/threads:1_mean                               942 ns        942 ns
bm_to_string_naive_strftime_snprintf/real_time/threads:4_mean                               272 ns       1070 ns

bm_format_naive_strftime_snprintf/real_time/threads:1_mean                                  762 ns        762 ns
bm_format_naive_strftime_snprintf/real_time/threads:4_mean                                  223 ns        873 ns

bm_formate_date/real_time/threads:1_mean                                                    689 ns        689 ns
bm_formate_date/real_time/threads:4_mean                                                    173 ns        691 ns

bm_format_date_karma1/real_time/threads:1_mean                                             1041 ns       1041 ns
bm_format_date_karma1/real_time/threads:4_mean                                              267 ns       1069 ns

bm_format_date_karma2/real_time/threads:1_mean                                              988 ns        988 ns
bm_format_date_karma2/real_time/threads:4_mean                                              245 ns        980 ns

bm_format_date_counting_lookup/real_time/threads:1_mean                                     156 ns        156 ns
bm_format_date_counting_lookup/real_time/threads:4_mean                                      41 ns        162 ns

bm_format_date_not_counting_lookup/real_time/threads:1_mean                                 100 ns        100 ns
bm_format_date_not_counting_lookup/real_time/threads:4_mean                                  25 ns         98 ns
```

However our code got bigger. We now have 3 custom formatting functions - which means we are going to use more instruction level cache. Depending on circumstances it might be an issue or not. There is only one option - profile, benchmark, learn.

Can we go any faster? It turns out that the answer is yes. At least in certain scenarios. More in [part 4]({{< relref "get_that_date_p4.md">}}).
