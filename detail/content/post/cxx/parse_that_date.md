---
title: "Parse that date"
date: "2016-10-09"
tags: 
    - "c++"
    - "performance-tuning"
---

In the [previous mini-series]({{< relref "get_that_date_p4.md">}}) of posts I was looking at various techniques for improving performance of converting time expressed as a number of microseconds since epoch into a string of a given format, for example:
```
1474913552123456 -> 20160926T18:12:32.123456Z
```

If we are forced to use special techniques for creating datetime strings then it is almost certain that we have to handle the opposite case as well. How to parse a datetime string into a number of microseconds since epoch? How to do it quickly without slowing down the critical path of the application? In languages with GC we also want to minimise GC pressure (one of the next posts will show similar code adapted to low-latency and low-GC Java).

**EDIT:** Most recent code samples and reviewed benchmark code available [here](https://bitbucket.org/mh321/time-experiments/overview).

<!--more-->

# Naïve solution
Our string has microsecond (and we might want to parse nanoseconds as well) component. Because of that we cannot use [`get_time`](http://en.cppreference.com/w/cpp/io/manip/get_time) to parse the whole input because `get_time`'s output is [`std::tm`](http://en.cppreference.com/w/cpp/chrono/c/tm) which can store up to seconds precision. What are our alternatives?

## `sscanf` individual components
We could use [`sscanf`](http://en.cppreference.com/w/cpp/io/c/fscanf) to get the individual date/time components and then convert it to epoch timestamp. Example below uses [date](https://github.com/HowardHinnant/date) library to do the final conversion:

```cpp
std::chrono::microseconds BENCHMARK_NOINLINE parse_date_naive(const char * str)
{
    int y, m, d, h, min, sec, usec;
    if(7 != sscanf(str, "%4d%2d%2dT%d:%d:%d.%d", &y, &m, &d, &h, &min, &sec, &usec))
    {
        return std::chrono::microseconds{0};
    }
    using namespace std::chrono;
    using namespace date;
    const auto ymd = year(y)/m/d;
    time_point<system_clock, microseconds> tp = sys_days(ymd) +
            hours{h} + minutes{min} + seconds{sec} + microseconds(usec);
    return tp.time_since_epoch();
}
```

It works, but the code doesn't look that nice. Performance-wise it is also not that great:

```
BM_parse/parse_date_naive_PREDICTABLE_INPUT/real_time/threads:1    1437 ns       1437 ns 
BM_parse/parse_date_naive_PREDICTABLE_INPUT/real_time/threads:4     350 ns       1399 ns 
BM_parse/parse_date_naive_RANDOM_WALK_INPUT/real_time/threads:1    1449 ns       1449 ns 
BM_parse/parse_date_naive_RANDOM_WALK_INPUT/real_time/threads:4     366 ns       1461 ns 
BM_parse/parse_date_naive_RANDOM_INPUT/real_time/threads:1         1513 ns       1513 ns 
BM_parse/parse_date_naive_RANDOM_INPUT/real_time/threads:4          376 ns       1505 ns 
```

## `boost::posix_time` parsing
If we can use Boost, then there is another way of parsing the datetime string. Depending on the point of view this solution might be considered somewhat cleaner or... maybe not:

```cpp
std::chrono::microseconds BENCHMARK_NOINLINE parse_date_ptime(const char * str)
{
    namespace bpt = boost::posix_time;
    static const bpt::ptime time_t_epoch{boost::gregorian::date{1970,1,1}};
    thread_local std::stringstream ss = [](){
        using boost::posix_time::time_input_facet;
        using std::locale;

        std::stringstream result;
        result.imbue(locale{locale::classic(), new time_input_facet{"%Y%m%dT%H:%M:%S%F"}});
        return result;
    }();

    ss.clear();
    ss.str(str);
    bpt::ptime time;
    ss >> time;

    const auto diff = time - time_t_epoch;
#ifdef BOOST_DATE_TIME_POSIX_TIME_STD_CONFIG
    return std::chrono::nanoseconds{diff.ticks()};
#else
    return std::chrono::microseconds{diff.ticks()};
#endif
}
```

There are some steps necessary to setup the `stringstream` to deal with our time format. And performance is rather poor (also note the difference between multi-threaded and single-threaded variants):

```
BM_parse/parse_date_ptime_PREDICTABLE_INPUT/real_time/threads:1   2043 ns       2043 ns
BM_parse/parse_date_ptime_PREDICTABLE_INPUT/real_time/threads:4    875 ns       3502 ns
BM_parse/parse_date_ptime_RANDOM_WALK_INPUT/real_time/threads:1   2082 ns       2082 ns
BM_parse/parse_date_ptime_RANDOM_WALK_INPUT/real_time/threads:4    869 ns       3478 ns
BM_parse/parse_date_ptime_RANDOM_INPUT/real_time/threads:1        2133 ns       2133 ns
BM_parse/parse_date_ptime_RANDOM_INPUT/real_time/threads:4         884 ns       3536 ns
```


# Custom parsing
For a particular low-latency / high-throughput application we know exactly what our input format is going to look like. Therefore it makes sense to roll out a custom parser. What is the key component that is needed? We need to be able to parse a certain number of digits (i.e. `4`, `2` and `6`) into a number. We could use the following customised unrolled routine that should deliver decent performance:

```cpp
constexpr static int POW10_LOOKUP[] = {
    1,
    10,
    100,
    1000,
    10000,
    100000,
    1000000,
    10000000,
    100000000,
    1000000000
};

template<int N>
__attribute__((optimize("unroll-loops")))
inline int atou(const char * str)
{
    // Precondition: str has a positive string that fits in an int
    unsigned result = 0;
    for(auto i = 0; i != N; ++i)
    {
        result += (str[i] - '0') * POW10_LOOKUP[N-i-1];
    }
    return result;
}
```

If we need error checking we could check whether each `char` is actually a digit and uses `error_code`, magic value or exception to signalise problems.

Now equipped with the number parse the easiest thing to do is to reuse the code presented in the first example with `sscanf` solution:

```cpp
std::chrono::microseconds BENCHMARK_NOINLINE parse_date(const char * str)
{
    // Preconditions: str points to beginning of correctly formatted string (at least sufficient length)

    using namespace std::chrono;
    using namespace date;
    const auto y = atou<4>(str + YEAR_OFFSET);
    const auto m = atou<2>(str + MONTH_OFFSET);
    const auto d = atou<2>(str + DAY_OFFSET);
    const auto h = atou<2>(str + HOURS_OFFSET);
    const auto min = atou<2>(str + MINUTES_OFFSET);
    const auto sec = atou<2>(str + SECONDS_OFFSET);
    const auto usec = atou<6>(str + USEC_OFFSET);

    const auto ymd = year(y)/m/d;
    time_point<system_clock, microseconds> tp = sys_days(ymd) +
            hours{h} + minutes{min} + seconds{sec} + microseconds(usec);
    return tp.time_since_epoch();
}
```

And performance difference is astounding:

```
BM_parse/parse_date_PREDICTABLE_INPUT/real_time/threads:1      56 ns         56 ns
BM_parse/parse_date_PREDICTABLE_INPUT/real_time/threads:4      13 ns         52 ns
BM_parse/parse_date_RANDOM_WALK_INPUT/real_time/threads:1      53 ns         53 ns
BM_parse/parse_date_RANDOM_WALK_INPUT/real_time/threads:4      13 ns         52 ns
BM_parse/parse_date_RANDOM_INPUT/real_time/threads:1           56 ns         56 ns
BM_parse/parse_date_RANDOM_INPUT/real_time/threads:4           14 ns         55 ns
```

# Custom parsing with predictable input

Quite often we know something more about the input data to parse. For example if processing some form of sending times or transaction times it is likely that they will increase monotonically and the date component (`year`, `month`, `day`) is seldom going to change (e.g. once per day). If that is the case I can cache the `ymd` component and recalculate it only if needed (very similar to the one used in [date formatting example]({{< relref "get_that_date_p4.md">}})):

```cpp
std::chrono::microseconds BENCHMARK_NOINLINE heuristic_parse_date(const char * str)
{
    // Preconditions: str points to beginning of correctly formatted string (at least sufficient length)
    using namespace std::chrono;
    using namespace date;
    constexpr static auto YMD_LENGTH = 8;
    thread_local char last_ymd[YMD_LENGTH];
    thread_local time_point<system_clock, microseconds> last_ymd_tp;

    if(BOOST_UNLIKELY(std::memcmp(last_ymd, str, YMD_LENGTH)) != 0)
    {
        const auto y = atou<4>(str + YEAR_OFFSET);
        const auto m = atou<2>(str + MONTH_OFFSET);
        const auto d = atou<2>(str + DAY_OFFSET);
        last_ymd_tp = sys_days(year(y)/m/d);
        std::memcpy(last_ymd, str, YMD_LENGTH);
    }
    const auto h = atou<2>(str + HOURS_OFFSET);
    const auto min = atou<2>(str + MINUTES_OFFSET);
    const auto sec = atou<2>(str + SECONDS_OFFSET);
    const auto usec = atou<6>(str + USEC_OFFSET);
    time_point<system_clock, microseconds> tp = last_ymd_tp + hours{h} + minutes{min} + seconds{sec} + microseconds(usec);

    return tp.time_since_epoch();
}
```

In some cases it is better to make separate parser objects, each with its own state instead of a global function. That makes sense if parsing multiple data streams with dates differing in one of `ymd` components.


This does seem to work well if we are not dealing with completely random input:

```
BM_parse/heuristic_parse_date_PREDICTABLE_INPUT/real_time/threads:1     37 ns         37 ns
BM_parse/heuristic_parse_date_PREDICTABLE_INPUT/real_time/threads:4      9 ns         37 ns
BM_parse/heuristic_parse_date_RANDOM_WALK_INPUT/real_time/threads:1     40 ns         40 ns
BM_parse/heuristic_parse_date_RANDOM_WALK_INPUT/real_time/threads:4     10 ns         40 ns
BM_parse/heuristic_parse_date_RANDOM_INPUT/real_time/threads:1          73 ns         73 ns
BM_parse/heuristic_parse_date_RANDOM_INPUT/real_time/threads:4          18 ns         74 ns

```

