---
title: "Get that date (part 1)"
date: "2016-09-24"
tags: 
    - "c++"
    - "performance-tuning"
---

# What are we trying to do?

Representing time as number of timeunits (e.g. seconds/milliseconds/microseconds or even nanoseconds) elapsed since some fixed point in time (e.g. [Unix time](https://en.wikipedia.org/wiki/Unix_time)) is fine in most applications. It is cheap to obtain these timestamps using [`clock_gettime`](https://linux.die.net/man/3/clock_gettime) or its variation. It can be used in the critical path of the application and non-critical path tools (e.g. log viewer) could convert it to human readable time if need be.

But in some cases it there is really no other choice and it has to be done on the critical path of the program execution. For example when we have to communicate with an external system that expects timestamps in form of strings adhering to a certain format. Let's consider the format below which is a variation of [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601): `20160926T18:12:32.123456Z`. Some more info about this timestamp:
```cpp
// input:  1474913552123456
// 
// output:
//
// 0         1         2    
// 0123456789012345678901234   <- length
// 20160926T18:12:32.123456Z   <- timestamp
```

**EDIT:** Most recent code samples and reviewed benchmark code available [here](https://bitbucket.org/mh321/time-experiments/overview).

<!--more-->

# Naïve / obvious solution

A rather naïve that could equally well (with some minor tweaks) in C. I'm going to use [`gmtime_r`](http://en.cppreference.com/w/c/chrono/gmtime) which consumes a given time since epoch (`time_t`) and populates the calendar time (UTC) in [`tm`](http://en.cppreference.com/w/c/chrono/tm) format:
```cpp
struct tm 
{
    int tm_sec;         /* seconds */
    int tm_min;         /* minutes */
    int tm_hour;        /* hours */
    int tm_mday;        /* day of the month */
    int tm_mon;         /* month */
    int tm_year;        /* year */
    int tm_wday;        /* day of the week */
    int tm_yday;        /* day in the year */
    int tm_isdst;       /* daylight saving time */
};
```

The code below could do the job:
```cpp
#include <chrono>
#include <ctime>
#include <cstdio>
#include <string>

constexpr static auto DATE_TIME_LENGTH = 17;
constexpr static auto TIMESTAMP_LENGTH = 25;

std::string BENCHMARK_NOINLINE to_string_naive_strftime_snprintf(std::chrono::microseconds micros)
{
    const auto sec = static_cast<time_t>(micros.count() / 1000000);
    const auto usec = micros.count() % 1000000;

    std::tm tm;
    bool ok = gmtime_r(&sec, &tm);


    char buffer[32];
    ok &= std::strftime(buffer, sizeof(buffer), "%Y%m%dT%H:%M:%S", &tm);
    ok &= std::snprintf(buffer + DATE_TIME_LENGTH, sizeof(buffer) - DATE_TIME_LENGTH, ".%06ldZ", usec) > 0;
    return std::string(buffer, BOOST_LIKELY(ok) ? TIMESTAMP_LENGTH : 0);
}

```
- Error handling could be modified to suit one's taste. We could throw exceptions, return empty string, use `error_code`, etc. This does not matter for now in this exercise.
- For more info about branch hints have a look at the [early SG-14 branch hints proposal](https://ctrychta.github.io/branch_hints_proposal.html)
- `std::chrono::microseconds` could be replaced with `int64_t` if `chrono` not available, but this could lead to *funny* runtime problems

**The question is how good or how bad is the code above.**

# Benchmarking and profiling

[`perf`](https://perf.wiki.kernel.org/index.php/Main_Page) is [one of the great tools](http://www.brendangregg.com/linuxperf.html) for inferring about the different aspects of performance. However we need to exercise our function in order to profile it. For this exercise I'm going to use the [benchmark lib](https://github.com/google/benchmark) from Google.

It is recommended to disable the CPU scaling when benchmarking. One way of doing that in modern RedHat like distributions is to use `tuned`:
```bash
$ sudo tuned-adm profile latency-performance
```

I'm going to use the following function to drive the benchmark (hoping that it avoids many of the microbenchmark pitfalls):
```cpp
void to_string_naive_strftime_snprintf(benchmark::State & state)
{
    auto start = 1474913552000000;
    while(state.KeepRunning())
    {
        auto res = to_string_strftime_snprintf(++start);
        // Do something with the result so that it doesn't get optimised away completely...
        if(BOOST_UNLIKELY(res[8] != 'T' || res[23] != (start % 10 + '0'))) { std::abort();}
    }
}
BENCHMARK(to_string_naive_strftime_snprintf)->Threads(1)->UseRealTime();\
BENCHMARK(to_string_naive_strftime_snprintf)->Threads(4)->UseRealTime();\
```

What are the results when running the code?
```bash
$ perf record -g -d ./time_benchmarks  --benchmark_min_time=2 --benchmark_repetitions=5 --benchmark_filter="to_string_naive_strftime_snprintf"
Run on (24 X 2262 MHz CPU s)
2016-10-01 13:14:46
Benchmark                                                Time           
----------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------
bm_to_string_naive_strftime_snprintf/real_time/threads:1               942 ns        942 ns   
bm_to_string_naive_strftime_snprintf/real_time/threads:1               942 ns        942 ns   
bm_to_string_naive_strftime_snprintf/real_time/threads:1               943 ns        942 ns   
bm_to_string_naive_strftime_snprintf/real_time/threads:1               942 ns        942 ns   
bm_to_string_naive_strftime_snprintf/real_time/threads:1               943 ns        943 ns   
bm_to_string_naive_strftime_snprintf/real_time/threads:1_mean          942 ns        942 ns   
bm_to_string_naive_strftime_snprintf/real_time/threads:1_stddev          1 ns          1 ns   
bm_to_string_naive_strftime_snprintf/real_time/threads:4               272 ns       1071 ns   
bm_to_string_naive_strftime_snprintf/real_time/threads:4               273 ns       1077 ns   
bm_to_string_naive_strftime_snprintf/real_time/threads:4               270 ns       1067 ns   
bm_to_string_naive_strftime_snprintf/real_time/threads:4               272 ns       1071 ns   
bm_to_string_naive_strftime_snprintf/real_time/threads:4               270 ns       1066 ns   
bm_to_string_naive_strftime_snprintf/real_time/threads:4_mean          272 ns       1070 ns   
bm_to_string_naive_strftime_snprintf/real_time/threads:4_stddev          1 ns          4 ns   
```

Where do we spend most of time?
```bash
$ perf report -f -g 'graph,0.5,caller'
-   22.98%     0.98%  time_benchmarks  time_benchmarks     [.] bm_to_string_naive_strftime_snprintf
   - 22.00% bm_to_string_naive_strftime_snprintf
        7.95% __tz_convert
      - 5.15% to_string_naive_strftime_snprintf[abi:cxx11]
           1.95% malloc
           0.75% std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >::_M_construct<char const*>
           0.52% __memcpy_sse2_unaligned
        2.77% __offtime
        1.83% __snprintf
        1.02% __strftime_internal
        0.93% __tzfile_compute
        0.62% _IO_vsnprintf
        0.52% __lll_lock_wait_private
```

Not really **that** unexpected. We spend a lot of time converting the epoch seconds into the actual time and then we also have to spent some time producing the formatted strings. Somewhat unexpected might be the that we do some allocations and spend time creating the `basic_string` objects. I'm using `GCC` and the desired timestamp is likely too long to benefit from [SSO](https://github.com/elliotgoodrich/SSO-23/blob/master/README.md). Finally we see that there is some locking going on underneath and that is visible in the benchmark results for single-threaded and multi-threaded code.

[Part 2]({{< relref "get_that_date_p2.md">}}) will look at the basic steps to optimise this naïve solution
